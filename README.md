Docz.py
=======

This is a simple script that can be used to insert webbugs into docx files.

Supply it with two parameters..
* 1  The path to the docx file to insert the bug into.
* 2  The bug url (where to call back to, including parameters).

For example your command might look like this.

python docz.py /root/passwords.docx http://prometheaninfosec.com/webbug/?target=site\&type=css

NOTE: If you use the '&' character in your URL you need to escape it with a backslash.

If you have any questions you can ask me on Twitter @zaeyx.

